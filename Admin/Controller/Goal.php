<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Admin\Controller;

use apathy\DailyGoal\Entity\Goal as Entity;
use apathy\DailyGoal\Entity\Goal as EntityGoal;
use apathy\DailyGoal\Goal\AbstractGoal;
use apathy\DailyGoal\Repository\Goal as RepositoryGoal;
use apathy\DailyGoal\Repository\Streak;
use XF;
use XF\Admin\Controller\AbstractController;
use XF\ControllerPlugin\Style;
use XF\ControllerPlugin\Toggle;
use XF\Http\Request;
use XF\Mvc\FormAction;
use XF\Mvc\ParameterBag;
use XF\Mvc\Reply\Message;
use XF\Mvc\Reply\Redirect;
use XF\Mvc\Reply\View;
use XF\Repository\StyleProperty;
use XF\Util\Arr;
use XF\Util\Color;

/**
 * Class Goal
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Admin\Controller
 */

class Goal extends AbstractController
{
	/**
	 * @return View
	 */
	public function actionIndex(): View
	{
		$goals = $this->getGoalRepo()->findGoalsForList();

		$viewParams = [
			'goals' => $goals,
		];

		return $this->view('apathy\DailyGoal:Goal', 'ap_dg_goal_list', $viewParams);
	}

	/**
	 * @param Entity $goal
	 *
	 * @return View
	 */
	public function actionAddEdit(Entity $goal)
	{
		$app = $this->app();

		$page = $this->filterPage();
		$perPage = $app->options->apDgStreakPageLimit;

		$streakRepo = $this->getStreakRepo();

		// Find goal history
		$goalHistory = $streakRepo->findGoalHistory($goal->content_type, true)->order('date', 'DESC');
		$goalHistory = $goalHistory->limitByPage($page, $perPage);

		// Count longest streak of type
		$longestStreakItems = $streakRepo->findGoalHistory($goal->content_type, true)->fetch();
		$longestStreak = $streakRepo->calculateLongestStreak($longestStreakItems);

		// Fetch style properties if they exist
		$style = $this->plugin('XF:Style')->getActiveEditStyle();

		$props = $this->getRelevantStyleProperties($goal);
		$props = array_values($props->toArray());

		if ($props === [])
		{
			$goalProps = $goal->getStyleProperties();

			$props = [
				['property_value' => $goalProps['bar_color']],
				['property_value' => $goalProps['line_color']],
			];
		}

		$viewParams = [
			'goal'          => $goal,
			'history'       => $goalHistory->fetch(),
			'page'          => $page,
			'perPage'       => $perPage,
			'total'         => $goalHistory->total(),
			'longestStreak' => $longestStreak,
			'activeStyle'   => $style,
			'props'         => $props,
		];

		return $this->view('apathy\DailyGoal:Goal\Edit', 'ap_dg_goal_edit', $viewParams);
	}

	/**
	 * @return View
	 */
	public function actionAdd()
	{
		$type = $this->filter('type', 'str');
		$repo = $this->getGoalRepo();

		if ($type)
		{
			$goal = $repo->findGoalOfType($type);

			if ($goal instanceof EntityGoal)
			{
				return $this->error(\XF::phrase('ap_dg_this_goal_already_exists'));
			}

			$handler = $repo->getHandler($type);
			$goal = $this->initializeNewGoal($type, $handler->getTitle()->render());

			return $this->actionAddEdit($goal);
		}

		$viewParams = [
			'type'      => $type,
			'goalTypes' => $repo->getGoalTypes(),
		];

		return $this->view('apathy\DailyGoal:Goal\Chooser', 'ap_dg_goal_chooser', $viewParams);
	}

	/**
	 * @param ParameterBag $params
	 *
	 * @return Redirect|View
	 */
	public function actionDelete(ParameterBag $params)
	{
		$goal = $this->assertViewableGoal($params['goal_id']);
		$confirmed = $this->filter('confirmed', 'bool');

		if (!$confirmed)
		{
			$viewParams = [
				'confirmUrl'   => $this->buildLink('daily-goals/delete', $goal),
				'goalTitle'    => $goal->title,
				'resetWarning' => \XF::phrase('ap_dg_you_will_lose_all_history_entries_for_this_goal'),
			];

			return $this->view('apathy\DailyGoal:Goal\Delete', 'ap_dg_delete_confirm', $viewParams);
		}

		if ($this->filter('delete_history', 'bool'))
		{
			$repo = $this->getStreakRepo();
			$streaks = $repo->findGoalHistory($goal->content_type);

			foreach ($streaks AS $streak)
			{
				$streak->delete();
			}
		}

		$props = $this->getRelevantStyleProperties($goal);

		foreach ($props AS $prop)
		{
			$prop->delete();
		}

		$goal->delete();

		return $this->redirect($this->buildLink('daily-goals'));
	}


	/**
	 * @param ParameterBag $params
	 *
	 * @return View
	 */
	public function actionEdit(ParameterBag $params)
	{
		$goal = $this->assertViewableGoal($params->goal_id);

		return $this->actionAddEdit($goal);
	}

	/**
	 * @param ParameterBag $params
	 *
	 * @return Redirect
	 */
	public function actionSave(ParameterBag $params)
	{
		$this->assertPostOnly();

		if ($params->goal_id)
		{
			$goal = $this->assertViewableGoal($params->goal_id);
		}
		else
		{
			$goal = $this->app()->em()->create('apathy\DailyGoal:Goal');
		}

		$this->goalSaveProcess($goal)->run();

		return $this->redirect($this->buildLink('daily-goals/edit', $goal));
	}

	/**
	 * @return Message
	 */
	public function actionToggle()
	{
		/** @var Toggle $plugin */
		$plugin = $this->plugin('XF:Toggle');

		return $plugin->actionToggle('apathy\DailyGoal:Goal');
	}

	/**
	 * @return Redirect
	 */
	public function actionQuickCount()
	{
		$this->app()->simpleCache();

		$repo = $this->getGoalRepo();
		$goals = $repo->findActiveGoalsForList();
		$time = $repo->getTime();

		/** @var EntityGoal $goal */
		foreach ($goals AS $goal)
		{
			$goal->calculateGoalProgress($time);
		}

		return $this->redirect($this->getDynamicRedirect());
	}

	/**
	 * @param ParameterBag $params
	 *
	 * @return Redirect|View
	 */
	public function actionResetStreak(ParameterBag $params)
	{
		$goal = $this->assertViewableGoal($params['goal_id']);

		$confirmed = $this->filter('confirmed', 'bool');

		if (!$confirmed)
		{
			$viewParams = [
				'confirmUrl'   => $this->buildLink('daily-goals/reset-streak', $goal),
				'goalTitle'    => $goal->title,
				'resetWarning' => \XF::phrase('ap_dg_you_will_lose_all_history_entries_for_this_goal'),
			];

			return $this->view('apathy\DailyGoal:Goal\ResetStreak', 'ap_dg_reset_streak_confirm', $viewParams);
		}

		$streakRepo = $this->getStreakRepo();
		$streaks = $streakRepo->findGoalHistory($goal->content_type);

		foreach ($streaks AS $streak)
		{
			$streak->delete();
		}

		return $this->redirect($this->buildLink('daily-goals/edit', $goal));
	}

	/**
	 * @param string $type
	 * @param string $title
	 *
	 * @return EntityGoal
	 */
	protected function initializeNewGoal(string $type, string $title): EntityGoal
	{
		$goal = $this->app()->em()->create('apathy\DailyGoal:Goal');

		$goal->title = $title;
		$goal->content_type = $type;

		return $goal;
	}

	/**
	 * @param Entity $goal
	 *
	 * @return FormAction
	 */
	protected function goalSaveProcess(Entity $goal): FormAction
	{
		$form = $this->formAction();
		$repo = $this->getGoalRepo();

		$input = $this->filter([
			'goal'         => 'uint',
			'minimum_goal' => 'uint',
			'timeframe'    => 'uint',
			'weight'       => 'uint',
			'content_type' => 'str',
			'bar_color'    => 'str',
			'line_color'   => 'str',
			'options'      => 'array',
		]);

		$handlers = $repo->getHandlers();
		$handler = $handlers[$input['content_type']];

		$goal->title = $handler->getTitle()->render();

		if ($goal->isInsert())
		{
			$input['fa_icon'] = $handler->getFontAwesomeIcon();
		}

		// Verify colors
		foreach (['bar_color', 'line_color'] AS $color)
		{
			if (!$this->verifyColor($input[$color]))
			{
				return $form;
			}
		}

		// Save colors as style property values
		$colors = [$input['bar_color'], $input['line_color']];

		$this->propertySaveProcess($input['content_type'], $colors, $goal->title);

		unset($input['bar_color'], $input['line_color']);

		// Validate options
		$request = new Request($this->app->inputFilterer(), $input['options'], [], []);

		/** @var AbstractGoal $handler */
		if ($handler && !empty($input['options']))
		{
			$options = $handler->verifyOptions($request, $input['options'], $error);
			$options = Arr::filterRecursive($options);

			$input['options'] = $options;
		}

		$form->basicEntitySave($goal, $input);

		return $form;
	}

	/**
	 * @param string $contentType
	 * @param array $values
	 * @param string $title
	 */
	protected function propertySaveProcess(string $contentType, array $values, string $title)
	{
		$contentTypes = $this->formatType($contentType);

		/** @var Style $plugin */
		$plugin = $this->plugin('XF:Style');
		$style = $plugin->getActiveEditStyle();

		/** @var StyleProperty $propertyRepo */
		$propertyRepo = $this->repository('XF:StyleProperty');

		$props = [
			$contentTypes[0] => [
				'title' => $title . ' bar color',
				'value' => $values[0],
			],
			$contentTypes[1] => [
				'title' => $title . ' line color',
				'value' => $values[1],
			],
		];

		$displayOrder = 150;

		for ($i = 0; $i <= 1; $i++)
		{
			$prop = $this->assertViewableStyleProperty($contentTypes[$i], $style->style_id);

			if ($prop)
			{
				$propertyRepo->updatePropertyValues($style, [$contentTypes[$i] => $props[$contentTypes[$i]]['value']]);
			}
			else
			{
				$displayOrder += 50;
				$property = $this->app()->em()->create('XF:StyleProperty');

				$property->style_id       = $style->style_id;
				$property->property_name  = $contentTypes[$i];
				$property->group_name     = 'ap_daily_goals';
				$property->title          = $props[$contentTypes[$i]]['title'];
				$property->property_type  = 'value';
				$property->value_type     = 'color';
				$property->value_group    = 'Graph';
				$property->property_value = $props[$contentTypes[$i]]['value'];
				$property->display_order  = $displayOrder;
				$property->addon_id       = 'apathy/DailyGoal';

				$property->save();
			}
		}
	}

	protected function getRelevantStyleProperties(Entity $goal)
	{
		$contentType = $goal->content_type;
		$props = $this->formatType($contentType);

		/** @var Style $stylePlugin */
		$stylePlugin = $this->plugin('XF:Style');
		$style = $stylePlugin->getActiveEditStyle();

		return $this->assertViewableStyleProperty([$props[0], $props[1]], $style->style_id);
	}

	/**
	 * @param string $contentType
	 *
	 * @return array
	 */
	protected function formatType(string $contentType): array
	{
		$contentType = XF\Util\Php::camelCase($contentType);

		$barColor  = rtrim('apDgBarColor' . $contentType, 's') . 's';
		$lineColor = rtrim('apDgLineColor' . $contentType, 's') . 's';

		return [$barColor, $lineColor];
	}

	/**
	 * @param string|array $color
	 *
	 * @throws PrintableException
	 *
	 * @return boolean
	 */
	protected function verifyColor(string $color): bool
	{
		if ($color === '' || $color === '0')
		{
			return true;
		}

		$validator = new Color();

		$color = trim($color);

		if ($color === '')
		{
			return true;
		}
		else
		{
			if (!$validator->isValidColor($color))
			{
				throw new XF\PrintableException(\XF::phrase('ap_dg_please_enter_valid_color'));
			}
		}

		return true;
	}

	/**
	 * @return RepositoryGoal
	 */
	protected function getGoalRepo(): RepositoryGoal
	{
		return $this->repository('apathy\DailyGoal:Goal');
	}

	/**
	 * @return Streak
	 */
	protected function getStreakRepo(): Streak
	{
		return $this->repository('apathy\DailyGoal:Streak');
	}

	/**
	 * @param integer $goalId
	 * @param array|string|null $with
	 *
	 * @return EntityGoal|null
	 */
	protected function assertViewableGoal(int $goalId, $with = null): ?EntityGoal
	{
		return $this->assertRecordExists('apathy\DailyGoal:Goal', $goalId, $with, 'ap_dg_goal_not_found');
	}

	/**
	 * @param string|array $propertyName
	 * @param integer $styleId
	 */
	protected function assertViewableStyleProperty($propertyName, int $styleId)
	{
		$finder = $this->finder('XF:StyleProperty')->where('style_id', $styleId);

		if (is_array($propertyName))
		{
			foreach ($propertyName AS $name)
			{
				$whereOr[] = ['property_name', $name];
			}

			return $finder->whereOr($whereOr)->fetch();
		}

		return $finder->where('property_name', $propertyName)->fetchOne();
	}
}
