<?php

declare(strict_types=1);

use PhpCsFixer\Finder;
use XFCsFixer\ConfigHelper;

$finder = Finder::create()
	->in(__DIR__)
	->ignoreVCSIgnored(true)
	->notPath([
		// you may set paths or path patterns to exclude here
	]);

$config = (new ConfigHelper($finder))->getConfig();

// you may customize the configuration object further here

return $config;