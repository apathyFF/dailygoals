<?php

use Rector\CodeQuality\Rector\Class_\CompleteDynamicPropertiesRector;
use Rector\CodeQuality\Rector\If_\CompleteMissingIfElseBracketRector;
use Rector\CodeQuality\Rector\If_\ShortenElseIfRector;
use Rector\Config\RectorConfig;
use Rector\DeadCode\Rector\ClassMethod\RemoveNullTagValueNodeRector;
use Rector\DeadCode\Rector\ClassMethod\RemoveUselessParamTagRector;
use Rector\DeadCode\Rector\ClassMethod\RemoveUselessReturnTagRector;
use Rector\DeadCode\Rector\Property\RemoveUselessVarTagRector;
use Rector\DeadCode\Rector\StaticCall\RemoveParentCallWithoutParentRector;
use Rector\TypeDeclaration\Rector\Property\TypedPropertyFromStrictConstructorRector;

return RectorConfig::configure()
	->withRules([
		TypedPropertyFromStrictConstructorRector::class,
	])
	->withPreparedSets(
		deadCode: true,
		codeQuality: true,
		codingStyle: true
	)
	->withSkip([
		// Don't interact with libraries
		__DIR__ . '/tools',
		__DIR__ . '/vendor',

		// Preference
		CompleteDynamicPropertiesRector::class,
		ShortenElseIfRector::class,

		// Disable these rules as they remove our docblocks
		RemoveUselessParamTagRector::class,
		RemoveUselessVarTagRector::class,
		RemoveUselessReturnTagRector::class,
		RemoveNullTagValueNodeRector::class,
		RemoveParentCallWithoutParentRector::class,
		CompleteMissingIfElseBracketRector::class,
	]);
/** @todo uncomment when we hard require XF2.3 */
//->withPhpSets(php80: true);
