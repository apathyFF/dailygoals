<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal;

use XF\AddOn\AddOn;
use XF\App;
use XF\Container;
use XF\Template\Templater;

class Listener
{
	/**
	 * @param App $app
	 */
	public static function appSetup(App $app)
	{
		$container = $app->container();

		$container['ap.dgGoalStyles'] = $app->fromRegistry('apDgGoalStyles', function (Container $c)
		{
			return $c['em']->getRepository('apathy\DailyGoal:Goal')->rebuildGoalStyleCache();
		});
	}

	/**
	 * If a supported addon has been uninstalled then its goal/streak history should be
	 * pruned as well.
	 *
	 * @param AddOn $addOn
	 * @param string $addonId
	 * @param array $json
	 *
	 * @return void
	 */
	public static function addonPostUninstall(AddOn $addOn, string $addonId, array $json)
	{
		/** @var Goal $goalRepo */
		$goalRepo = \XF::repository('apathy\DailyGoal:Goal');
		$handlers = $goalRepo->getHandlers();

		foreach ($handlers AS $handler)
		{
			if ($addonId == $handler->getAddonId())
			{
				$goal = \XF::finder('apathy\DailyGoal:Goal')->where('content_type', $handler->getGoalType())->fetchOne();

				if (!$goal)
				{
					continue;
				}

				foreach ($goal->History AS $streakHistory)
				{
					$streakHistory->delete();
				}

				$goal->delete();
			}
		}
	}

	/**
	 * @param Templater $templater
	 * @param $type
	 * @param $template
	 * @param $name
	 * @param array $arguments
	 * @param array $globalVars
	 */
	public static function templaterMacroPreRender(
		Templater $templater,
		&$type,
		&$template,
		&$name,
		array &$arguments,
		array &$globalVars
	)
	{
		if (!empty($arguments['group']) && $arguments['group']->group_id == 'ap_daily_goal')
		{
			$template = 'ap_dg_option_macros';
		}
	}
}
