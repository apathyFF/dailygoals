<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Option;

use XF\Entity\CronEntry;
use XF\Entity\Option;
use XF\Option\AbstractOption;

/**
 * Class Goal
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Option\Goal
 */

class Goal extends AbstractOption
{
	/**
	 * @param $value
	 * @param Option $option
	 *
	 * @return boolean
	 */
	public static function updateCronTimer(&$value, Option $option): bool
	{
		self::setMidnightResetTimer($value);

		$app = \XF::app();

		/** @var \apathy\DailyGoal\Repository\Goal $repo */
		$repo = $app->repository('apathy\DailyGoal:Goal');
		$goals = $repo->findActiveGoalsForList();
		$time = $repo->getTime($value);

		$app->simpleCache();

		/** @var \apathy\DailyGoal\Entity\Goal $goal */
		foreach ($goals AS $goal)
		{
			$goal->calculateGoalProgress($time);
		}

		return true;
	}

	/**
	 * Sets the timer for the counter reset job
	 *
	 * @param string $periodicity
	 *
	 * @return boolean
	 */
	protected static function setMidnightResetTimer(&$periodicity): bool
	{
		/** @var CronEntry $job */
		$job = \XF::finder('XF:CronEntry')
			->where('cron_method', 'resetCounterAtMidnight')
			->where('addon_id', 'apathy/DailyGoal')
			->fetchOne();

		if (!$job)
		{
			return true;
		}

		$runRules = self::getRunRules($periodicity);
		$runRules['hours'] = [0];
		$runRules['minutes'] = [30];

		$job->run_rules = $runRules;
		$job->save(true, false);

		return true;
	}

	/**
	 * @param string $periodicity
	 *
	 * @return array
	 */
	protected static function getRunRules(string $periodicity): array
	{
		switch ($periodicity)
		{
			case 'daily':   return ['day_type' => 'dow', 'dow' => [-1]];
			case 'weekly':  return ['day_type' => 'dow', 'dow' => [1]];
			case 'monthly': return ['day_type' => 'dom', 'dom' => [1]];
		}
	}
}
