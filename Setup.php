<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;

/**
 * Class Setup
 *
 * @author apathy <https://fortreeforums.xyz/>
 *
 * @package \apathy\DailyGoal
 */

class Setup extends AbstractSetup
{
	use StepRunnerInstallTrait;
	use StepRunnerUpgradeTrait;
	use StepRunnerUninstallTrait;

	use Install\Table;
	use Install\Widget;

	use Install\Upgrade1060070;
	use Install\Upgrade2040070;
	use Install\Upgrade2050070;
	use Install\Upgrade2060030;
	use Install\Upgrade2060130;

	/**
	 *
	 */
	public function installStep1()
	{
		$schemaManager = $this->schemaManager();
		$tables = $this->getTables();

		foreach ($tables AS $name => $data)
		{
			$schemaManager->createTable($name, $data);
		}
	}

	/**
	 *
	 */
	public function uninstallStep1()
	{
		$schemaManager = $this->schemaManager();
		$tableNames = array_keys($this->getTables());

		foreach ($tableNames AS $name)
		{
			$schemaManager->dropTable($name);
		}

		// Unset SimpleCache array
		unset($this->app()->simpleCache['apathy/DailyGoal']);
	}

	/**
	 *
	 */
	public function installStep2()
	{
		$widgets = $this->getWidgets();

		foreach ($widgets AS $widgetId => $widget)
		{
			$this->createWidget($widgetId, $widget[0], [
				'positions' => $widget['positions'],
				'options'   => $widget['options'],
			]);
		}
	}

	/**
	 *
	 */
	public function uninstallStep2()
	{
		$widgets = array_keys($this->getWidgets());

		foreach ($widgets AS $widget)
		{
			$this->deleteWidget($widget);
		}
	}
}
