<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Repository;

use apathy\DailyGoal\Entity\Goal;
use XF\Mvc\Entity\ArrayCollection;
use XF\Mvc\Entity\Finder;
use XF\Mvc\Entity\Repository;

/**
 * Class Streak
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Repository
 */

class Streak extends Repository
{
	/**
	 * @param string $contentType
	 * @param boolean $activeOnly
	 * @param boolean $limitByPage
	 * @param integer $page
	 *
	 * @return Finder
	 */
	public function findGoalHistory(
		string $contentType = '',
		$activeOnly = false,
		$limitByPage = false,
		$page = 0
	): Finder
	{
		$history = $this->finder('apathy\DailyGoal:History');

		if ($activeOnly)
		{
			$history = $history->with('Goal')->where('Goal.active', 1);
		}

		$history = $history->where('periodicity', $this->options()->apDgPeriodicity);

		if ($contentType !== '' && $contentType !== '0')
		{
			$history = $history->where('content_type', $contentType);
		}

		if ($limitByPage)
		{
			$history = $history->limitByPage($page, $this->app()->options()->apDgStreakPageLimit);
		}

		return $history;
	}

	/**
	 * @param integer $startTime
	 * @param integer $graphRange
	 * @param array $filters
	 *
	 * @return Finder
	 */
	public function findGoalHistoryForGraph(int $startTime, int $graphRange, $filters = []): Finder
	{
		$history = $this->finder('apathy\DailyGoal:History')
			->with('Goal')
			->where('Goal.active', 1)
			->where('periodicity', $this->options()->apDgPeriodicity)
			->where('date', '>=', ($startTime - $graphRange))
			->order('date', 'asc');

		if (!empty($filters))
		{
			$history = $history->whereOr($filters);
		}

		return $history;
	}

	/**
	 * @param ArrayCollection $entity
	 * @param integer $streak
	 *
	 * @return array
	 */
	public function calculateLongestStreak(ArrayCollection $entity, int $streak = 0): array
	{
		$longest = [
			'count'     => 0,
			'endDate'   => 0,
			'startDate' => 0,
			'last'      => 0,
		];

		$currentPeriod = 0;

		/** @var Goal $goal */
		foreach ($entity AS $goal)
		{
			if ($currentPeriod === 0)
			{
				$currentPeriod = ($goal->periodicity !== 'daily')
					? $goal->start_date
					: $goal['date'];
			}

			if ($goal['fulfilled'])
			{
				if ($streak >= 1 && $streak >= $longest['count'])
				{
					$longest['startDate'] = $currentPeriod;
					$longest['endDate'] = $goal['date'];
				}

				$streak++;
			}
			else
			{
				$streak = 0;
				$currentPeriod = 0;
			}

			if ($streak > $longest['count'])
			{
				$longest['count'] = $streak;
			}

			$longest['last'] = $streak;
		}

		return $longest;
	}

	/**
	 * @param string $type
	 * @param integer $timeframe
	 *
	 * @return array
	 */
	public function countTotalStreaksOfType(string $type, int $timeframe): array
	{
		$goals = $this->findGoalHistory($type)->fetch();

		$total = [
			'streak'    => 0,
			'periods'   => 0,
			'fulfilled' => 0,
		];

		$currentPeriod = 0;

		/** @var Goal $goal */
		foreach ($goals AS $goal)
		{
			if ($goal['fulfilled'] && $currentPeriod == $timeframe)
			{
				$total['streak']++;
			}

			if ($goal['fulfilled'])
			{
				$currentPeriod++;
				$total['fulfilled']++;
			}
			else
			{
				$currentPeriod = 0;
			}

			$total['periods']++;
		}

		return $total;
	}
}
