<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Repository;

use apathy\DailyGoal\Entity\Goal as GoalEntity;
use apathy\DailyGoal\Goal\AbstractGoal;
use XF;
use XF\Mvc\Entity\AbstractCollection;
use XF\Mvc\Entity\Repository;
use XF\Repository\Style;

/**
 * Class Goal
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Repository
 */

class Goal extends Repository
{
	/**
	 * Fetches all goals, regardless of whether they're active or not
	 *
	 * @return AbstractCollection
	 */
	public function findGoalsForList(): AbstractCollection
	{
		return $this->finder('apathy\DailyGoal:Goal')->fetch();
	}

	/**
	 * @return AbstractCollection
	 */
	public function findActiveGoalsForList(): AbstractCollection
	{
		return $this->finder('apathy\DailyGoal:Goal')->where('active', 1)->fetch();
	}

	/**
	 * @param string $type
	 *
	 * @return GoalEntity|null
	 */
	public function findGoalOfType(string $type): ?GoalEntity
	{
		return $this->finder('apathy\DailyGoal:Goal')->where('content_type', $type)->fetchOne();
	}

	/**
	 * @param string $type
	 * @param GoalEntity|null $goal
	 *
	 * @return AbstractGoal|null
	 */
	public function getHandler(string $type, ?GoalEntity $goal = null): ?AbstractGoal
	{
		$app = $this->app();
		$class = $app->getContentTypeFieldValue($type, 'ap_dg_goal_handler_class');

		if (!class_exists($class))
		{
			return null;
		}

		$class = $app->extendClass($class);

		return new $class($app, $goal);
	}

	/**
	 * @return array
	 */
	public function getHandlers(): array
	{
		$app = $this->app();
		$types = $app->getContentTypeField('ap_dg_goal_handler_class');

		$output = [];

		foreach ($types AS $type => $class)
		{
			if (class_exists($class))
			{
				$class = $app->extendClass($class);
				$output[$type] = new $class($app);
			}
		}

		return $output;
	}

	/**
	 * @return array
	 */
	public function getGoalTypes(): array
	{
		$goals = $this->getHandlers();

		$types = [];

		/** @var AbstractGoal $goal */
		foreach ($goals AS $goal)
		{
			if ($goal->isEnabled())
			{
				$types[$goal->getGoalType()] = $goal->getTitle();
			}
		}

		return $types;
	}

	/**
	 * @param GoalEntity $goal
	 */
	public function adjustGoal(GoalEntity $goal)
	{
		$result = $this->finder('apathy\DailyGoal:History')
			->where('content_type', $goal['content_type'])
			->order('date', 'desc')
			->fetch($goal['timeframe']);

		$broken = 0;
		$streak = 0;

		foreach ($result AS $current)
		{
			if ($current['fulfilled'])
			{
				$broken = 0;
				$streak++;
			}
			else
			{
				$broken++;
				$streak = 0;
			}
		}

		$value = $goal['goal'];

		if ($current['fulfilled'] && $streak >= $goal['timeframe'])
		{
			$goal->goal = ($value + $goal['weight']);
		}
		else if (
			!$current['fulfilled']
			&& $broken >= $goal['timeframe']
			&& $value > $goal['minimum_goal']
		)
		{
			$goal->goal = max($goal['minimum_goal'], ($value - $goal['weight']));
		}

		$goal->save();
	}

	/**
	 * @param GoalEntity $goal
	 */
	public function resetGoal(GoalEntity $goal)
	{
		$app = $this->app();
		$time = $this->getTime();

		// Find the previous days results, since the job
		// will run at 00:30am the next day
		$time['end'] = $time['start'];
		$time['start'] -= 86400;

		$periodEndTotal = $goal->calculateGoalProgress($time);

		$this->saveStatResult(
			$time['current'],
			$goal->content_type,
			$periodEndTotal,
			$goal->goal,
			($periodEndTotal >= $goal->goal)
		);

		if (!$app->options->apDgDiableAutoAdjustment)
		{
			$this->adjustGoal($goal);
		}

		$goal->setSimpleCacheValue(0);
		$goal->fastUpdate('notification_posted', 0);
	}

	/**
	 * @param integer $time
	 * @param string $type
	 * @param integer $total
	 * @param integer $goal
	 * @param boolean $fulfilled
	 */
	public function saveStatResult(int $time, string $type, int $total, int $goal, bool $fulfilled)
	{
		$entity = $this->app()->em()->create('apathy\DailyGoal:History');

		$input = [
			'date'         => $time,
			'content_type' => $type,
			'counter'      => $total,
			'goal'         => $goal,
			'fulfilled'    => $fulfilled,
		];

		$entity->bulkSet($input);
		$entity->save(true, false);
	}

	/**
	 * @param string $periodicity
	 * @param string $time
	 *
	 * @return string
	 */
	public function getTimeStartByPeriodicity(string $periodicity, string $time): string
	{
		return match ($periodicity)
		{
			'weekly'  => strtotime('last sunday midnight', $time),
			'monthly' => strtotime(date('Y-m-01 00:00:00', $time)),
			'daily'   => $time - ($time % 86400),
			default   => $time - ($time % 86400),
		};
	}

	/**
	 * @param string $periodicity
	 * @param string $time
	 *
	 * @return string
	 */
	public function getTimeEndByPeriodicity(string $periodicity, string $time): string
	{
		return match ($periodicity)
		{
			'weekly'  => strtotime('next sunday midnight', $time),
			'monthly' => strtotime('+1 month', $time),
			'daily'   => $time + 86400,
			default   => $time + 86400,
		};
	}

	/**
	 * We need to pass periodicity as a value in our periodicity
	 * option validator.
	 *
	 * @return array
	 */
	public function getTime(string $periodicity = ''): array
	{
		$time = \XF::$time;

		if ($periodicity === '' || $periodicity === '0')
		{
			// Fall back to "daily" periodicity if the option can't be found
			// (like during the upgrade process)
			$periodicity = isset($this->options()->apDgPeriodicity)
				? $this->options()->apDgPeriodicity
				: 'daily';
		}

		$start = $this->getTimeStartByPeriodicity($periodicity, $time);
		$end = $this->getTimeEndByPeriodicity($periodicity, $start);

		return [
			'current' => $time,
			'start'   => $start,
			'end'     => $end,
		];
	}

	/**
	 * @return array
	 */
	public function rebuildGoalStyleCache(): array
	{
		$app = $this->app();
		$goals = $this->findActiveGoalsForList();

		$styles = [];

		/** @var GoalEntity $goal */
		foreach ($goals AS $goal)
		{
			// This needs to match the output of the values in findStreakTypes() otherwise we can't get per-line coloring
			$phrase = $app->language()->renderPhrase($goal->getHandler()->getStreakPhraseId());

			$contentType = XF\Util\Php::camelCase($goal->content_type);
			$name = rtrim($contentType, 's') . 's';

			$styles[$goal->content_type] = [
				'bar_color'     => 'apDgBarColor' . $name,
				'line_color'    => 'apDgLineColor' . $name,
				'formattedName' => $phrase,
			];
		}

		$app->registry()->set('apDgGoalStyles', $styles);

		/** @var Style $styleRepo */
		$styleRepo = $this->repository('XF:Style');
		$styleRepo->updateAllStylesLastModifiedDate();

		return $styles;
	}

	/**
	 * @param GoalEntity $goal
	 * @param integer $progress
	 * @param string $condition
	 */
	public function prepareSiropuNotification(GoalEntity $goal, int $progress, string $condition = 'fulfilled')
	{
		$options = $this->options();
		$handler = $goal->getHandler();

		$input['goal_progress'] = $progress;

		if ($handler->canPostSiropuChatNotifications() && $options->apDgSiropuChatNotificationTime == $condition)
		{
			$input += [
				'message_user_id'  => 0,
				'message_username' => $options->apDgSiropuChatBotName,
				'message_room_id'  => $options->apDgSiropuChatNotifierRoom,
				'message_bot_name' => $options->apDgSiropuChatBotName,
				'message_type'     => 'bot',
				'message_column'   => 'message_text',
				'date_column'      => 'message_date',
				'entity'           => 'Siropu\Chat:Message',
			];

			$this->postSiropuNotification($goal, $input);
		}
		else if ($handler->canPostSiropuShoutboxNotifications() && $options->apDgSiropuShoutboxNotificationTime == $condition)
		{
			$input += [
				'shout_user_id'  => $options->apDgSiropuShoutboxBotID,
				'message_column' => 'shout_message',
				'date_column'    => 'shout_date',
				'entity'         => 'Siropu\Shoutbox:Shout',
			];

			$this->postSiropuNotification($goal, $input);
		}
	}


	/**
	 * Post a goal fulfillment notification the Siropu Chat or Siropu Shoutbox
	 *
	 * @param GoalEntity $goal
	 * @param array $options
	 *
	 * @return boolean
	 */
	public function postSiropuNotification(GoalEntity $goal, array $options): bool
	{
		if ($options['goal_progress'] < $goal->goal || $goal->notification_posted)
		{
			return false;
		}

		$time = $this->getTime();
		$start = $time['start'];
		$end = $time['end'];

		$search = [
			'%TITLE%',
			'%GOAL%',
			'%PROGRESS%',
		];

		$replace = [
			$goal->title,
			$goal->goal,
			$options['goal_progress'],
		];

		$response = str_replace($search, $replace, $this->options()->apDgSiropuChatBotMessage);
		$options[$options['message_column']] = $response;

		$message = $this->finder($options['entity'])->where($options['message_column'], $response);
		$message = $message->where($options['date_column'], '>=', $start)->where($options['date_column'], '<=', $end)->fetchOne();

		if ($message)
		{
			return false;
		}

		$message = $this->em->create($options['entity']);

		$message->bulkSetIgnore($options);
		$message->save();

		$goal->fastUpdate('notification_posted', 1);

		return true;
	}
}
