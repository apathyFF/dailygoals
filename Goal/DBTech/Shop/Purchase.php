<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Goal\DBTech\Shop;

use apathy\DailyGoal\Goal\AbstractGoal;
use DBTech\Shop\Repository\Item;
use XF\Phrase;

/**
 * Class Purchase
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Goal\DBTech\Shop
 */

class Purchase extends AbstractGoal
{
	/**
	 * @return string
	 */
	public function getGoalType(): string
	{
		return 'dbtech_shop_purchase';
	}

	/**
	 * @return string
	 */
	public function getAddonId(): string
	{
		return 'DBTech/Shop';
	}

	/**
	 * @return array
	 */
	public function getStyleProperties(): array
	{
		return ['bar_color' => '#98cafb', 'line_color' => '#98cafb'];
	}

	/**
	 * @return string
	 */
	public function getFontAwesomeIcon(): string
	{
		return 'fa-shopping-basket';
	}

	/**
	 * @return string|Phrase
	 */
	public function getTitle()
	{
		return \XF::phrase('dbtech_shop_purchases');
	}

	/**
	 * @return array
	 */
	public function getTemplateParams(): array
	{
		$goal = $this->goal;

		$templater = $this->app()->templater();

		/** @var Item $repo */
		$repo  = $this->repository('DBTech\Shop:Item');

		$items = $repo->findItemsForList();

		$choices[] = [
			'value' => '',
			'label' => \XF::phrase('none'),
		];

		foreach ($items AS $item)
		{
			$choices[$item->item_id] = [
				'value' => $item->item_id,
				'label' => $item->title,
			];
		}

		$controlOptions = [
			'name'     => 'options[purchase_item]',
			'value'    => $goal->options['purchase_item'],
			'multiple' => true,
			'size'     => 8,
		];

		$selectItems = $templater->formSelectRow(
			$controlOptions,
			$choices,
			['label' => \XF::phrase('ap_dg_goal_options.count_specific_item')]
		);

		return [
			'goal'        => $goal,
			'selectItems' => $selectItems,
		];
	}

	/**
	 * @param integer $start
	 * @param integer $end
	 *
	 * @return integer
	 */
	public function calculateProgressTowardsGoal(int $start, int $end): int
	{
		$finder = $this->finder('DBTech\Shop:Purchase')
			->where('dateline', '>=', $start)
			->where('dateline', '<=', $end)
			->where('hidden', false);

		$goal = $this->goal;

		if (isset($goal->options['purchase_item']) && !empty($goal->options['purchase_item']))
		{
			foreach ($goal->options['purchase_item'] AS $item)
			{
				$finder = $finder->where('item_id', $item);
			}
		}

		return $finder->fetch()->count();
	}
}
