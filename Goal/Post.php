<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Goal;

use XF\Http\Request;
use XF\Mvc\Entity\Finder;
use XF\Phrase;
use XF\Repository\Node;

/**
 * Class Post
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Goal
 */

class Post extends AbstractGoal
{
	use OptionsTrait;

	/**
	 * @return string
	 */
	public function getGoalType(): string
	{
		return 'post';
	}

	/**
	 * @return array
	 */
	public function getStyleProperties(): array
	{
		return ['bar_color' => '#9898fb', 'line_color' => '#9898fb'];
	}

	/**
	 * @return string
	 */
	public function getFontAwesomeIcon(): string
	{
		return 'fa-comments';
	}

	/**
	 * @return string|Phrase
	 */
	public function getTitle()
	{
		return \XF::phrase('posts');
	}

	/**
	 * @return array
	 */
	public function getTemplateParams(): array
	{
		$goal = $this->goal;

		/** @var Node $nodeRepo */
		$nodeRepo = $this->repository('XF:Node');
		$nodes = $nodeRepo->getNodeOptionsData(true, 'Forum');
		$excludedNodes = $this->createItemsSelectRow($goal, $nodes, 'node_id');

		// Setup "Include comments in goal" option
		$includeComments = $this->createCheckbox($goal, 'include_comments');

		return [
			'goal'            => $goal,
			'includeComments' => $includeComments,
			'excludedNodes'   => $excludedNodes,
		];
	}

	/**
	 * @param Request $request
	 * @param array $options
	 * @param $error
	 *
	 * @return array
	 */
	public function verifyOptions(Request $request, array &$options, &$error = null): array
	{
		return $request->filter([
			'node_id'          => 'array-uint',
			'include_comments' => 'bool',
		]);
	}

	/**
	 * We need to handle counting differently depending on which comment addon we're using.
	 *
	 * [OzzModz] Post Comments integrates with the Post entity, and
	 * can be considered regular posts, whereas [UW] Forum Commment
	 * System uses it's own entity.
	 *
	 * @param integer $start
	 * @param integer $end
	 *
	 * @return integer
	 */
	public function calculateProgressTowardsGoal(int $start, int $end): int
	{
		$goal = $this->goal;

		$excludedNodes = 0;

		if (isset($goal->options['node_id']))
		{
			$excludedNodes = $goal->options['node_id'];
		}

		$postCount = $this->countPostsMadeToday($start, $end, $excludedNodes);
		$commentCount = 0;

		if ($this->canCalculateComments())
		{
			$comments =  $this->countCommentsMadeToday(
				$postCount,
				$start,
				$end,
				$excludedNodes
			);

			if ($comments instanceof Finder)
			{
				$commentCount = $comments->fetch()->count();
			}
		}

		$postCount = $postCount->fetch()->count();

		return $postCount + $commentCount;
	}

	/**
	 * @param integer $start
	 * @param integer $end
	 * @param array|integer $excludedNodes
	 *
	 * @return Finder
	 */
	protected function countPostsMadeToday(int $start, int $end, $excludedNodes): Finder
	{
		$postCount = $this->finder('XF:Post')->with('Thread')
			->where('post_date', '>=', $start)
			->where('post_date', '<=', $end)
			->where('message_state', 'visible');

		if ($excludedNodes > 0)
		{
			$postCount = $postCount->where('Thread.node_id', '!=', $excludedNodes);
		}

		if ($this->hasAddonInstalled('ThemeHouse/PostComments') && !$this->canCalculateComments())
		{
			return $postCount->where('thpostcomments_depth', '<', 1);
		}

		return $postCount;
	}

	/**
	 * @param Finder $postCount
	 * @param integer $start
	 * @param integer $end
	 * @param array|integer $excludedNodes
	 *
	 * @return Finder|null
	 */
	protected function countCommentsMadeToday(Finder $postCount, int $start, int $end, $excludedNodes): ?Finder
	{
		if ($this->hasAddonInstalled('ThemeHouse/PostComments'))
		{
			return $postCount->where('thpostcomments_depth', '>=', 1);
		}

		if ($this->hasAddonInstalled('UW/FCS'))
		{
			return $this->countUwForumComments($start, $end, $excludedNodes);
		}

		return null;
	}

	/**
	 * @return boolean
	 */
	protected function canCalculateComments(): bool
	{
		$goal = $this->goal;

		if (!isset($goal->options['include_comments']))
		{
			return false;
		}

		return !(!$goal->options['include_comments'] && !$this->hasAddonInstalled('ThemeHouse/PostComments') && !$this->hasAddonInstalled('UW/FCS'));
	}

	/**
	 * @param integer $start
	 * @param integer $end
	 * @param array|integer $excludedNodes
	 *
	 * @return Finder|null
	 */
	protected function countUwForumComments(int $start, int $end, $excludedNodes): ?Finder
	{
		$finder = $this->finder('UW\FCS:Comment')->with('Thread')
			->where('comment_date', '>=', $start)
			->where('comment_date', '<=', $end);

		if ($excludedNodes > 0)
		{
			$finder = $finder->where('Thread.node_id', '!=', $excludedNodes);
		}

		return $finder;
	}
}
