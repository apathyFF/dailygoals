<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Goal\Siropu\Chat;

use apathy\DailyGoal\Goal\AbstractGoal;
use apathy\DailyGoal\Goal\OptionsTrait;
use XF\Http\Request;
use XF\Phrase;

/**
 * Class Message
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Goal\Siropu\Chat
 */

class Message extends AbstractGoal
{
	use OptionsTrait;

	/**
	 * @return string
	 */
	public function getGoalType(): string
	{
		return 'siropu_chat_room_message';
	}

	/**
	 * @return string
	 */
	public function getAddonId(): string
	{
		return 'Siropu/Chat';
	}

	/**
	 * @return array
	 */
	public function getStyleProperties(): array
	{
		return ['bar_color' => '#98fbe2', 'line_color' => '#98fbe2'];
	}

	/**
	 * @return string
	 */
	public function getFontAwesomeIcon(): string
	{
		return 'fa-comments';
	}

	/**
	 * @return string|Phrase
	 */
	public function getTitle()
	{
		return \XF::phrase('ap_dg_chat_messages');
	}

	/**
	 * @return array
	 */
	public function getTemplateParams(): array
	{
		$goal = $this->goal;

		/** @var Room $roomRepo */
		$roomRepo = $this->repository('Siropu\Chat:Room');
		$rooms = $roomRepo->findRoomsForList()->fetch()->toArray();
		$rooms = $this->getSelectItemsData($rooms, 'room_id', 'room_name');

		$excludedRooms = $this->createItemsSelectRow($goal, $rooms, 'room_id');

		return [
			'goal' => $goal,
			'excludedRooms' => $excludedRooms,
		];
	}

	/**
	 * @param Request $request
	 * @param array $options
	 * @param $error
	 *
	 * @return array
	 */
	public function verifyOptions(Request $request, array &$options, &$error = null): array
	{
		return ['room_id' => $request->filter('room_id', 'array-uint')];
	}

	/**
	 * @param integer $start
	 * @param integer $end
	 *
	 * @return integer
	 */
	public function calculateProgressTowardsGoal(int $start, int $end): int
	{
		$finder = $this->finder('Siropu\Chat:Message')
			->where('message_date', '>=', $start)
			->where('message_date', '<=', $end);

		if (isset($this->goal->options['room_id']) && !empty($this->goal->options['room_id']))
		{
			$finder = $finder->where('message_room_id', '!=', $this->goal->options['room_id']);
		}

		return $finder->fetch()->count();
	}
}
