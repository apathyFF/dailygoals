<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Goal\XenAddons\CAS;

use apathy\DailyGoal\Goal\AbstractGoal;
use XF\Phrase;

/**
 * Class AdItem
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Goal\XenAddons\CAS
 */

class AdItem extends AbstractGoal
{
	/**
	 * @return string
	 */
	public function getGoalType(): string
	{
		return 'cas_ad';
	}

	/**
	 * @return string
	 */
	public function getAddonId(): string
	{
		return 'XenAddons/CAS';
	}

	/**
	 * @return array
	 */
	public function getStyleProperties(): array
	{
		return ['bar_color' => '#cafb98', 'line_color' => '#cafb98'];
	}

	/**
	 * @return string
	 */
	public function getFontAwesomeIcon(): string
	{
		return 'fa-search-dollar';
	}

	/**
	 * @return string|Phrase
	 */
	public function getTitle()
	{
		return \XF::phrase('cas_ads');
	}

	/**
	 * @param integer $start
	 * @param integer $end
	 *
	 * @return integer
	 */
	public function calculateProgressTowardsGoal(int $start, int $end): int
	{
		return $this->finder('XenAddons\CAS:AdItem')
			->where('create_date', '>=', $start)
			->where('create_date', '<=', $end)
			->where('ad_state', 'visible')
			->fetch()
			->count();
	}
}
