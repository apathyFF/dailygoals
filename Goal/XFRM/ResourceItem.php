<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Goal\XFRM;

use apathy\DailyGoal\Goal\AbstractGoal;
use XF\Phrase;

/**
 * Class Resource
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Goal\XFRM
 */

class Resource extends AbstractGoal
{
	/**
	 * @return string
	 */
	public function getGoalType(): string
	{
		return 'resource';
	}

	/**
	 * @return string
	 */
	public function getAddonId(): string
	{
		return 'XFRM';
	}

	/**
	 * @return array
	 */
	public function getStyleProperties(): array
	{
		return ['bar_color' => '#fbfb98', 'line_color' => '#fbfb98'];
	}

	/**
	 * @return string
	 */
	public function getFontAwesomeIcon(): string
	{
		return 'fa-hdd';
	}

	/**
	 * @return string|Phrase
	 */
	public function getTitle()
	{
		return \XF::phrase('xfrm_resources');
	}

	/**
	 * @param integer $start
	 * @param integer $end
	 *
	 * @return integer
	 */
	public function calculateProgressTowardsGoal(int $start, int $end): int
	{
		return $this->finder('XFRM:ResourceItem')
			->where('resource_date', '>=', $start)
			->where('resource_date', '<=', $end)
			->where('resource_state', 'visible')
			->fetch()
			->count();
	}
}
