<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Goal;

use apathy\DailyGoal\Entity\Goal;

/**
 * Trait OptionsTrait
 *
 * Common options that can be used by many goals
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Goal
 */
trait OptionsTrait
{
	/**
	 * @param string $optionId
	 *
	 * @return string
	 */
	public function createCheckbox(Goal $goal, string $optionId): string
	{
		$controlOptions = [
			'name'  => 'options[' . $optionId . ']',
			'value' => $goal->options[$optionId],
		];

		$option[] =  [
			'name'     => 'options[' . $optionId . ']',
			'label'    => \XF::phrase('ap_dg_goal_options.' . $optionId),
			'hint'     => \XF::phrase('ap_dg_goal_options_hint.' . $optionId),
			'value'    => 1,
			'selected' => $goal->options[$optionId],
			'type'     => 'check',
		];

		return \XF::app()->templater()->formCheckboxRow($controlOptions, $option, []);
	}

	/**
	 * Creates a selectrow of nodes which can be excluded from the goal
	 *
	 * @param Goal|null $goal
	 * @param array $items
	 * @param string $optionId
	 * @param string $columnName
	 *
	 * @return string
	 */
	public function createItemsSelectRow(?Goal $goal, array $items, string $optionId = 'category_id'): string
	{
		$selected = [];

		if (isset($goal->options[$optionId]) && !empty($goal->options[$optionId]))
		{
			foreach ($goal->options[$optionId] AS $item)
			{
				if (isset($items[$item]))
				{
					$selected[] = $items[$item]['value'];
				}
			}
		}

		$controlOptions = [
			'name'     => 'options[' . $optionId . ']',
			'value'    => $selected,
			'multiple' => true,
			'size'     => 8,
		];

		return $this->app()->templater()->formSelectRow(
			$controlOptions,
			$items,
			['label' => \XF::phrase('ap_dg_goal_options.' . $optionId)]
		);
	}

	/**
	 * Returns an array of choices for a select dropdown
	 *
	 * @param $items
	 * @param string $columnIdName
	 *
	 * @return array
	 */
	public function getSelectItemsData($items, string $columnName = 'category_id', string $labelName = 'title'): array
	{
		$choices = [
			0 => ['_type' => 'option', 'value' => 0, 'label' => \XF::phrase('(none)')],
		];

		foreach ($items AS $item)
		{
			$choices[$item->{$columnName}] = [
				'value' => $item->{$columnName},
				'label' => $item->{$labelName},
			];
		}

		return $choices;
	}

}
