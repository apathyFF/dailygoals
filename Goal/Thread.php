<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Goal;

use XF\Http\Request;
use XF\Phrase;
use XF\Repository\Node;

/**
 * Class Thread
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Goal
 */

class Thread extends AbstractGoal
{
	use OptionsTrait;

	/**
	 * @return string
	 */
	public function getGoalType(): string
	{
		return 'thread';
	}

	/**
	 * @return array
	 */
	public function getStyleProperties(): array
	{
		return ['bar_color' => '#ca98fb', 'line_color' => '#ca98fb'];
	}

	/**
	 * @return string
	 */
	public function getFontAwesomeIcon(): string
	{
		return 'fa-comment';
	}

	/**
	 * @return string|Phrase
	 */
	public function getTitle()
	{
		return \XF::phrase('threads');
	}

	/**
	 * @return array
	 */
	public function getTemplateParams(): array
	{
		/** @var Node $nodeRepo */
		$nodeRepo = $this->repository('XF:Node');
		$nodes = $nodeRepo->getNodeOptionsData(true, 'Forum');
		$excludedNodes = $this->createItemsSelectRow($this->goal, $nodes, 'node_id');

		// Setup "Only count threads with replies"
		$repliesOnly = $this->createCheckbox($this->goal, 'replies_only');

		return [
			'excludedNodes' => $excludedNodes,
			'repliesOnly'   => $repliesOnly,
		];
	}

	/**
	 * @param Request $request
	 * @param array $options
	 * @param $error
	 *
	 * @return array
	 */
	public function verifyOptions(Request $request, array &$options, &$error = null): array
	{
		return $request->filter([
			'node_id'      => 'array-uint',
			'replies_only' => 'bool',
		]);
	}

	/**
	 * @param integer $start
	 * @param integer $end
	 *
	 * @return integer
	 */
	public function calculateProgressTowardsGoal(int $start, int $end): int
	{
		$goal = $this->goal;

		$finder = $this->finder('XF:Thread')
			->where('post_date', '>=', $start)
			->where('post_date', '<=', $end)
			->where('discussion_state', 'visible');

		if (isset($goal->options['node_id']))
		{
			$excludedNodes = $goal->options['node_id'];
			$finder = $finder->where('node_id', '!=', $excludedNodes);
		}

		if (isset($goal->options['replies_only']) && $goal->options['replies_only'])
		{
			$finder = $finder->where('reply_count', '>=', 1);
		}

		return $finder->fetch()->count();
	}
}
