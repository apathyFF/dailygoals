<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Goal;

use XF\Phrase;

/**
 * Class Trophy
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Goal
 */

class Trophy extends AbstractGoal
{
	/**
	 * @return string
	 */
	public function getGoalType(): string
	{
		return 'trophy';
	}

	/**
	 * @return array
	 */
	public function getStyleProperties(): array
	{
		return ['bar_color' => '#fb98e2', 'line_color' => '#fb98e2'];
	}

	/**
	 * @return string
	 */
	public function getFontAwesomeIcon(): string
	{
		return 'fa-trophy';
	}

	/**
	 * @return string|Phrase
	 */
	public function getTitle()
	{
		return \XF::phrase('trophies');
	}

	/**
	 * @return boolean
	 */
	public function isEnabled(): bool
	{
		return $this->app->options()->enableTrophies;
	}

	/**
	 * @param integer $start
	 * @param integer $end
	 *
	 * @return integer
	 */
	public function calculateProgressTowardsGoal(int $start, int $end): int
	{
		$this->app()->jobManager()->enqueueUnique('Trophy', 'XF:Trophy', [], false);

		return $this->finder('XF:UserTrophy')
			->where('award_date', '>=', $start)
			->where('award_date', '<=', $end)
			->fetch()
			->count();
	}
}
