<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Goal;

use apathy\DailyGoal\Entity\Goal;
use apathy\DailyGoal\XF\Entity\User;
use XF;
use XF\App;
use XF\Http\Request;
use XF\Mvc\Entity\Finder;
use XF\Mvc\Entity\Repository;
use XF\Phrase;

/**
 * Class AbstractGoal
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Goal
 */

abstract class AbstractGoal
{
	/**
	 * @var App $app
	 */
	protected $app;

	/**
	 * @var Goal|null
	 */
	protected ?Goal $goal;

	/**
	 * @var string
	 */
	public $goalType;

	/**
	 * @var array
	 */
	protected array $options = [];

	/**
	 * @param App $app
	 * @param Goal|null $goal
	 */
	public function __construct(App $app, ?Goal $goal = null)
	{
		$this->app = $app;

		$this->goal = $goal;
		$this->goalType = $this->getGoalType();
	}

	/**
	 * @return string
	 */
	abstract public function getGoalType(): string;

	/**
	 * @param integer $start
	 * @param integer $end
	 *
	 * @return integer
	 */
	abstract public function calculateProgressTowardsGoal(int $start, int $end): int;

	/**
	 * @return string
	 */
	abstract public function getFontAwesomeIcon(): string;

	/**
	 * @return string|Phrase
	 */
	abstract public function getTitle();

	/**
	 * @return string
	 */
	public function getAddonId(): string
	{
		return '';
	}

	/**
	 * @return string
	 */
	public function getFormattedGoalType(): string
	{
		return XF\Util\Php::camelCase($this->getGoalType());
	}

	/**
	 * @return boolean
	 */
	public function isEnabled(): bool
	{
		$addonId = $this->getAddonId();

		if ($addonId !== '' && $addonId !== '0')
		{
			return $this->hasAddonInstalled($addonId);
		}

		return true;
	}

	/**
	 * @return Goal
	 */
	public function getGoal(): Goal
	{
		return $this->goal;
	}

	/**
	 * @return string
	 */
	public function getSimpleCacheId(): string
	{
		return $this->getFormattedGoalType() . 'Count';
	}

	/**
	 * @return Phrase
	 */
	public function getGoalPhrase(): Phrase
	{
		$id = rtrim($this->goalType, 's') . 's';
		$id = 'ap_dg_goal_' . $id;

		return \XF::phrase($id);
	}

	/**
	 * @return string
	 */
	public function getStreakPhraseId(): string
	{
		return 'ap_dg_streak.' . $this->goalType;
	}

	/**
	 * @return Phrase
	 */
	public function getStreakPhrase(): Phrase
	{
		return \XF::phrase($this->getStreakPhraseId());
	}

	/**
	 * @return Phrase
	 */
	public function getLongestStreakPhrase(): Phrase
	{
		return \XF::phrase('ap_dg_longest_streak.' . $this->goalType);
	}

	/**
	 * @return string
	 */
	public function getPermissionId(): string
	{
		return 'view' . $this->getFormattedGoalType();
	}

	/**
	 * @return boolean
	 */
	public function getPermission(): bool
	{
		/** @var User $visitor */
		$visitor = \XF::visitor();

		return $visitor->canViewGoal($this->getPermissionId());
	}

	/**
	 * @return string
	 */
	public function getWidgetId(): string
	{
		return 'ap_dg_' . trim($this->getGoalType()) . '_streak';
	}

	/**
	 * @return integer|null
	 */
	public function getProgressValueForWidget(): ?int
	{
		return $this->app()->simpleCache()['apathy/DailyGoal'][$this->getSimpleCacheId()];
	}

	/**
	 * @return string
	 */
	public function getTemplate(): string
	{
		return 'ap_dg_' . $this->goalType . '_settings';
	}

	/**
	 * @return array
	 */
	public function getTemplateParams(): array
	{
		return [];
	}

	/**
	 * @return string
	 */
	public function renderTemplate(): string
	{
		$params = $this->getTemplateParams();

		return $this->app()->templater()->renderTemplate('admin:' . $this->getTemplate(), $params);
	}

	/**
	 * @param Request $request
	 * @param array $options
	 * @param $error
	 *
	 * @return array
	 */
	public function verifyOptions(Request $request, array &$options, &$error = null): array
	{
		return [];
	}


	/**
	 * @return boolean
	 */
	public function canPostSiropuChatNotifications(): bool
	{
		return $this->hasAddonInstalled('Siropu/Chat') && $this->app()->options->apDgSiropuChatNotifications;
	}

	/**
	 * @return boolean
	 */
	public function canPostSiropuShoutboxNotifications(): bool
	{
		return $this->hasAddonInstalled('Siropu/Shoutbox') && $this->app()->options->apDgSiropuShoutboxNotifications;
	}

	/**
	 * @param string $identifier
	 *
	 * @return boolean
	 */
	protected function hasAddonInstalled(string $identifier): bool
	{
		return array_key_exists($identifier, $this->app()->container('addon.cache'));
	}

	/**
	 * @param string $id
	 *
	 * @return Repository
	 */
	protected function repository(string $id): Repository
	{
		return $this->app()->repository($id);
	}

	/**
	 * @param string $id
	 *
	 * @return Finder
	 */
	protected function finder(string $id): Finder
	{
		return $this->app()->finder($id);
	}

	/**
	 * @return App
	 */
	protected function app(): App
	{
		return $this->app;
	}
}
