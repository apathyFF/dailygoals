<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Goal;

use XF\Http\Request;
use XF\Phrase;

/**
 * Class UserUpgrade
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Goal
 */

class UserUpgrade extends AbstractGoal
{
	use OptionsTrait;

	/**
	 * @return string
	 */
	public function getGoalType(): string
	{
		return 'user_upgrade';
	}

	/**
	 * @return array
	 */
	public function getStyleProperties(): array
	{
		return ['bar_color' => '#fb9898', 'line_color' => '#fb9898'];
	}

	/**
	 * @return string
	 */
	public function getFontAwesomeIcon(): string
	{
		return 'fa-level-up';
	}

	/**
	 * @return string|Phrase
	 */
	public function getTitle()
	{
		return \XF::phrase('user_upgrades');
	}

	/**
	 * @return array
	 */
	public function getTemplateParams(): array
	{
		$goal = $this->goal;

		/** @var XF\Repository\UserUpgrade $userUpgradeRepo */
		$userUpgradeRepo = $this->repository('XF:UserUpgrade');
		$upgrades = $userUpgradeRepo->findUserUpgradesForList()->fetch()->toArray();
		$upgrades = $this->getSelectItemsData($upgrades, 'user_upgrade_id');

		$limitedUpgrades = $this->createItemsSelectRow($goal, $upgrades, 'upgrade_id');

		return [
			'goal' => $goal,
			'limitedUpgrades' => $limitedUpgrades,
		];
	}

	/**
	 * @param Request $request
	 * @param array $options
	 * @param $error
	 *
	 * @return array
	 */
	public function verifyOptions(Request $request, array &$options, &$error = null): array
	{
		return ['upgrade_id' => $request->filter('upgrade_id', 'array-uint')];
	}

	/**
	 * @param integer $start
	 * @param integer $end
	 *
	 * @return integer
	 */
	public function calculateProgressTowardsGoal(int $start, int $end): int
	{
		$finder = $this->finder('XF:UserUpgradeActive')
			->where('start_date', '>=', $start)
			->where('start_date', '<=', $end);

		if (isset($this->goal->options['upgrade_id']))
		{
			$upgradeId = array_filter($this->goal->options['upgrade_id']);

			if ($upgradeId !== [])
			{
				$finder = $finder->where('user_upgrade_id', '=', $this->goal->options['upgrade_id']);
			}
		}

		return $finder->fetch()->count();
	}
}
