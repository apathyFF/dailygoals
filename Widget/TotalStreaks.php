<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Widget;

use apathy\DailyGoal\Repository\Goal;
use apathy\DailyGoal\Repository\Streak;
use XF\Widget\AbstractWidget;
use XF\Widget\WidgetRenderer;

/**
 * Class TotalStreaks
 *
 * @author apathy <https://fortreeforums.xyz/>
 *
 * @package \apathy\DailyGoal\Widget
 */

class TotalStreaks extends AbstractWidget
{
	/**
	 * @return WidgetRenderer|null
	 */
	public function render()
	{
		/** @var Goal $goalRepo */
		$goalRepo = $this->repository('apathy\DailyGoal:Goal');
		$goals = $goalRepo->findActiveGoalsForList();

		if (empty($goals))
		{
			return null;
		}

		/** @var Streak $streakRepo */
		$streakRepo = $this->repository('apathy\DailyGoal:Streak');

		$total = [];
		$totalStreaks = 0;

		/** @var \apathy\DailyGoal\Entity\Goal $goal */
		foreach ($goals AS $goal)
		{
			if ($goal->isActive() && $goal->isVisible())
			{
				$type = $goal['content_type'];
				$data = $streakRepo->countTotalStreaksOfType($type, $goal['timeframe']);

				$total[$type] = $data;
				$total[$type]['phrase'] = $goal['title'];

				if ($data['periods'] >= 1)
				{
					$totalStreaks++;
				}
			}
		}

		if ($total === [] || $totalStreaks === 0)
		{
			return null;
		}

		$viewParams = [
			'goals' => $goals,
			'title' => $this->getTitle() ?: $this->getDefaultTitle(),
			'total' => $total,
		];

		return $this->renderer('ap_dg_total_streaks_widget', $viewParams);
	}

	/**
	 * @return string|null
	 */
	public function getOptionsTemplate()
	{
		return '';
	}
}
