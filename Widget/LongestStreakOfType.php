<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Widget;

use apathy\DailyGoal\Entity\Goal;
use apathy\DailyGoal\Repository\Streak;
use XF\Widget\AbstractWidget;
use XF\Widget\WidgetRenderer;

/**
 * Class LongestStreakOfType
 *
 * @author apathy <https://fortreeforums.xyz/>
 *
 * @package \apathy\DailyGoal\Widget
 */

class LongestStreakOfType extends AbstractWidget
{
	/**
	 * @return WidgetRenderer|null
	 */
	public function render()
	{
		$params = $this->getContextParams();

		$definitionId = $this->widgetConfig->definitionId;
		$goals = $params['goals'];

		/** @var Streak $repo */
		$repo = $this->repository('apathy\DailyGoal:Streak');

		$longestStreak = 0;

		/** @var Goal $goal */
		foreach ($goals AS $goal)
		{
			if (
				$goal->isActive()
				&& $goal->isVisible()
				&& $goal->getHandler()->getWidgetId() === $definitionId
			)
			{
				$items = $repo->findGoalHistory($goal['content_type'])->fetch();
				$longestStreak = $repo->calculateLongestStreak($items);
				break;
			}
		}

		if ($longestStreak === 0)
		{
			return null;
		}

		$viewParams = [
			'goal'          => $goal,
			'longestStreak' => $longestStreak,
		];

		return $this->renderer('ap_dg_longest_streak_of_type_widget', $viewParams);
	}

	/**
	 * @return string|null
	 */
	public function getOptionsTemplate()
	{
		return '';
	}
}
