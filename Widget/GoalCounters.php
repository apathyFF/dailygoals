<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Widget;

use apathy\DailyGoal\Repository\Goal;
use apathy\DailyGoal\XF\Entity\User;
use XF\Phrase;
use XF\Widget\AbstractWidget;
use XF\Widget\WidgetRenderer;

class GoalCounters extends AbstractWidget
{
	/**
	 * @return WidgetRenderer|null
	 */
	public function render()
	{
		/** @var User $visitor */
		$visitor = \XF::visitor();

		if (!$visitor->canViewGoalWidget())
		{
			return null;
		}

		/** @var Goal $repo */
		$repo = $this->repository('apathy\DailyGoal:Goal');
		$goals = $repo->findActiveGoalsForList();

		if (empty($goals->toArray()))
		{
			return null;
		}

		$viewParams = [
			'goals' => $goals,
			'title' => $this->getTitle() ?: $this->getDefaultTitle(),
		];

		return $this->renderer('ap_dg_widget', $viewParams);
	}

	/**
	 * @return Phrase
	 */
	public function getTitle()
	{
		switch ($this->app->options()->apDgPeriodicity)
		{
			default:
			case 'daily':   $periodicity = \XF::phrase('daily')->render();
				break;
			case 'weekly':  $periodicity = \XF::phrase('weekly')->render();
				break;
			case 'monthly': $periodicity = \XF::phrase('monthly')->render();
				break;
		}

		return \XF::phrase('ap_dg_periodicity_x_goals', ['periodicity' => $periodicity]);
	}

	/**
	 * @return string|null
	 */
	public function getOptionsTemplate()
	{
		return '';
	}
}
