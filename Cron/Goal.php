<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Cron;

/**
 * Class Goal
 *
 * @author apathy <https://fortreeforums.xyz/>
 *
 * @package \apathy\DailyGoal\Cron
 */

class Goal
{
	/**
	 *
	 */
	public static function countGoalProgress()
	{
		$app = \XF::app();
		$app->simpleCache();

		/** @var \apathy\DailyGoal\Repository\Goal $repo */
		$repo = $app->repository('apathy\DailyGoal:Goal');

		$goals = $repo->findActiveGoalsForList();
		$time = $repo->getTime();

		/** @var \apathy\DailyGoal\Entity\Goal $goal */
		foreach ($goals AS $goal)
		{
			$goal->calculateGoalProgress($time);
		}
	}

	/**
	 *
	 */
	public static function deleteOlderHistoryItems()
	{
		$app = \XF::app();
		$timeframe = $app->options()->apDgDeleteHistoryItemsTimeframe;

		if ($timeframe == 0)
		{
			return;
		}

		$timeframe = strtotime(sprintf('-%s Days', $timeframe));

		$history = $app->finder('apathy\DailyGoal:History')->where('date', '<=', $timeframe)->fetch();

		foreach ($history AS $item)
		{
			$item->delete();
		}
	}

	/**
	 *
	 */
	public static function resetCounterAtMidnight()
	{
		/** @var \apathy\DailyGoal\Repository\Goal $repo */
		$repo = \XF::repository('apathy\DailyGoal:Goal');
		$goals = $repo->findActiveGoalsForList();

		foreach ($goals AS $goal)
		{
			$repo->resetGoal($goal);
		}
	}
}
