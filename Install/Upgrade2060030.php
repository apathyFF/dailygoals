<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Install;

use apathy\DailyGoal\Entity\Goal;
use XF\AddOn\AddOn;
use XF\App;

/**
 * @property AddOn addOn
 * @property App app
 *
 * @method \XF\Db\AbstractAdapter db()
 * @method \XF\Db\SchemaManager schemaManager()
 * @method \XF\Db\Schema\Column addOrChangeColumn($table, $name, $type = null, $length = null)
 */

trait upgrade2060030
{
	/**
	 *
	 */
	public function upgrade2060030Step1()
	{
		$goals = $this->app->finder('apathy\DailyGoal:Goal')->where('options', '!=', '[]')->where('options', '!=', '')->fetch();

		/** @var Goal $goal */
		foreach ($goals AS $goal)
		{
			$options = $goal->options;

			if (isset($options['excluded_nodes']))
			{
				$options['node_id'] = $options['excluded_nodes'];
				unset($options['excluded_nodes']);

				$goal->options = $options;
				$goal->save();
			}
		}
	}

	/**
	 *
	 */
	public function upgrade2060030Step2()
	{
		$db = $this->db();

		$db->query("
			UPDATE xf_permission
			SET    permission_group_id = 'apDailyGoals'
			WHERE  permission_group_id = 'ap_dailygoals'
			AND    addon_id            = 'apathy/DailyGoal'
		");

		$db->query("
			UPDATE xf_permission
			SET    depend_permission_id  = 'viewWidget'
			WHERE  permission_group_id   = 'apDailyGoals'
            AND    depend_permission_id != ''
			AND    addon_id              = 'apathy/DailyGoal'
		");

		$db->query("
			UPDATE xf_permission_entry
			SET permission_group_id   = 'apDailyGoals'
			WHERE permission_group_id = 'ap_dailygoals'
		");

		$tablesToUpdate = [
			'xf_permission',
			'xf_permission_entry',
			'xf_permission_entry_content',
		];

		$renames = [
			'apDgCanViewAmsGoal'      => 'viewAmsArticle',
			'apDgCanViewCasGoal'      => 'viewCasAd',
			'apDgCanViewChatGoal'     => 'viewSiropuChatRoomMessage',
			'apDgCanViewImsGoal'      => 'viewImsItem',
			'apDgCanViewLinkGoal'     => 'viewXaLdItem',
			'apDgCanViewMemberGoal'   => 'viewUser',
			'apDgCanViewPollGoal'     => 'viewPoll',
			'apDgCanViewPostGoal'     => 'viewPost',
			'apDgCanViewPurchaseGoal' => 'viewDbtechShopPurchase',
			'apDgCanViewRmsGoal'      => 'viewRmsItem',
			'apDgCanViewShoutGoal'    => 'viewSiropuShoutboxShout',
			'apDgCanViewShowcaseGoal' => 'viewScItem',
			'apDgCanViewStreaks'      => 'viewStreaks',
			'apDgCanViewThreadGoal'   => 'viewThread',
			'apDgCanViewTrophyGoal'   => 'viewTrophy',
			'apDgCanViewUbsGoal'      => 'viewBlogEntry',
			'apDgCanViewUpgradeGoal'  => 'viewUserUpgrade',
			'apDgCanViewWidget'       => 'viewWidget',
			'apDgCanViewXfrmGoal' 	  => 'viewResource',
			'apDgViewProfilePostGoal' => 'viewProfilePost',
		];

		foreach ($tablesToUpdate AS $tableName)
		{
			foreach ($renames AS $oldName => $newName)
			{
				$db->update(
					$tableName,
					['permission_id' => $newName],
					'permission_id = ? AND permission_group_id = ?',
					[$oldName, 'apDailyGoals']
				);
			}
		}
	}
}
