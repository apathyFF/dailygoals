<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Install;

use XF\AddOn\AddOn;
use XF\App;

/**
 * @property AddOn addOn
 * @property App app
 *
 * @method \XF\Db\AbstractAdapter db()
 * @method \XF\Db\SchemaManager schemaManager()
 * @method \XF\Db\Schema\Column addOrChangeColumn($table, $name, $type = null, $length = null)
 */

trait Widget
{
	/**
	 * @return array
	 */
	protected function getWidgets(): array
	{
		return [
			'ap_dg_daily_goals' => [
				'ap_daily_goal_widget',
				'positions' => [
					'forum_list_sidebar' => 1,
				],
				'options' => [
					'advanced_mode' => false,
					'template_title' => 'ap_dg_widget',
				],
			],
			'ap_dg_total_streaks' => [
				'ap_dg_total_streaks',
				'positions' => [
					'ap_dg_streaks_sidebar' => 1,
				],
				'options' => [
					'advanced_mode' => false,
					'template_title' => 'ap_dg_total_streaks_widget',
				],
			],
		];
	}
}
