<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Install;

use XF\AddOn\AddOn;
use XF\App;
use XF\Db\Schema\Create;

/**
 * @property AddOn addOn
 * @property App app
 *
 * @method \XF\Db\AbstractAdapter db()
 * @method \XF\Db\SchemaManager schemaManager()
 * @method \XF\Db\Schema\Column addOrChangeColumn($table, $name, $type = null, $length = null)
 */

trait upgrade2040070
{
	/**
	 *
	 */
	public function upgrade2040070Step1()
	{
		// Convert old stats_type to content types
		$types = [
			'post_goal'     => 'post',
			'thread_goal'   => 'thread',
			'member_goal'   => 'user',
			'ams_goal'      => 'ams_article',
			'ims_goal'      => 'ims_item',
			'link_goal'     => 'xa_ld_item',
			'rms_goal'      => 'rms_item',
			'ubs_goal'      => 'ubs_blog_entry',
			'cas_goal'      => 'cas_ad',
			'showcase_goal' => 'sc_item',
			'resource_goal' => 'resource',
		];

		foreach ($types AS $old => $new)
		{
			$this->app->db->query('UPDATE xf_ap_daily_goal_history SET stats_type = REPLACE(stats_type, ?, ?)', [$old, $new]);
		}

		$schemaManager = $this->schemaManager();

		if (!$schemaManager->tableExists('xf_ap_daily_goal_goal'))
		{
			$schemaManager->createTable('xf_ap_daily_goal_goal', function (Create $table)
			{
				$table->addColumn('goal_id', 'int')->autoIncrement();
				$table->addPrimaryKey('goal_id');
				$table->addColumn('title', 'varchar', 30)->setDefault('');
				$table->addColumn('fa_icon', 'varchar', 30)->setDefault('');
				$table->addColumn('content_type', 'varchar', 30)->setDefault('');
				$table->addColumn('goal', 'int')->nullable()->setDefault(5);
				$table->addColumn('minimum_goal', 'int')->nullable()->setDefault(3);
				$table->addColumn('timeframe', 'int')->nullable()->setDefault(3);
				$table->addColumn('weight', 'int')->nullable()->setDefault(5);
				$table->addColumn('options', 'blob')->nullable()->setDefault(null);
				$table->addColumn('active', 'bool')->setDefault(1);
			});

			$this->setupGoals();
		}
	}

	/**
	 *
	 */
	protected function setupGoals()
	{
		$app = $this->app();
		$db = $app->db();

		$handlers = $this->getHandlers();

		foreach ($handlers AS $handler)
		{
			if ($handler->isEnabled())
			{
				$handlerOptions = $handler->getOptions();

				$entity = $app->em()->create('apathy\DailyGoal:Goal');

				$db->beginTransaction();

				$entity->title        = $handler->getTitle();
				$entity->fa_icon      = $handler->getFontAwesomeIcon();
				$entity->content_type = $handlerOptions['content_type'];
				$entity->active       = 1;

				$setValues = $this->getAdminSetValues($handlerOptions['content_type']);

				foreach ($setValues AS $key => $value)
				{
					$entity[$key] = $value;
				}

				$entity->save(true, false);

				$db->commit();
			}
		}
	}

	/**
	 * Carry over the admin option values
	 * that admins have set instead of overwriting
	 * them with our default values
	 */
	protected function getAdminSetValues(string $type)
	{
		$options = \XF::options();

		$enabledGoals = [
			'apDgDisablePostGoal' => [
				'apDgPostGoal',
				'apDgMinPostGoal',
				'apDgAutoAdjustTimeframePosts',
				'apDgAutoAdjustWeightPosts',
				'apDgExcludedNodesPosts',
				'apDgIncludeComments',
			],
			'apDgDisableThreadGoal' => [
				'apDgThreadGoal',
				'apDgMinThreadGoal',
				'apDgAutoAdjustTimeframeThreads',
				'apDgAutoAdjustWeightThreads',
				'apDgExcludedNodesThreads',
			],
			'apDgDisableMemberGoal' => [
				'apDgMemberGoal',
				'apDgMinMemberGoal',
				'apDgAutoAdjustTimeframeMembers',
				'apDgAutoAdjustWeightMembers',
			],
			'apDgDisableAmsGoal' => [
				'apDgAmsGoal',
				'apDgMinAmsGoal',
				'apDgAutoAdjustTimeframeAms',
				'apDgAutoAdjustWeightAms',
			],
			'apDgDisableUbsGoal' => [
				'apDgUbsGoal',
				'apDgMinUbsGoal',
				'apDgAutoAdjustTimeframeUbs',
				'apDgAutoAdjustWeightUbs',
			],
			'apDgDisableScGoal' => [
				'apDgScGoal',
				'apDgMinScGoal',
				'apDgAutoAdjustTimeframeSc',
				'apDgAutoAdjustWeightSc',
			],
			'apDgDisableResourceGoal' => [
				'apDgResourceGoal',
				'apDgMinResourceGoal',
				'apDgAutoAdjustTimeframeResources',
				'apDgAutoAdjustWeightResources',
			],
			'apDgDisableCasGoal' => [
				'apDgCasGoal',
				'apDgMinCasGoal',
				'apDgAutoAdjustTimeframeCas',
				'apDgAutoAdjustWeightCas',
			],
			'apDgDisableRmsGoal' => [
				'apDgRmsGoal',
				'apDgMinRmsGoal',
				'apDgAutoAdjustTimeframeRms',
				'apDgAutoAdjustWeightRms',
			],
			'apDgDisableImsGoal' => [
				'apDgImsGoal',
				'apDgMinImsGoal',
				'apDgAutoAdjustTimeframeIms',
				'apDgAutoAdjustWeightIms',
			],
			'apDgDisableLinkGoal' => [
				'apDgLinkGoal',
				'apDgMinLinkGoal',
				'apDgAutoAdjustTimeframeLink',
				'apDgAutoAdjustWeightLink',
			],
		];

		$type = ucfirst($type);
		$type = explode('_', $type);
		$type = $type[0];

		$optionId = $type === 'User' ? 'apDgDisableMemberGoal' : 'apDgDisable' . $type . 'Goal';

		$optionValues = $enabledGoals[$optionId];

		$values = [];

		if (isset($options[$optionId]) && !$options[$optionId])
		{
			$values['goal'] = $options[$optionValues[0]];
			$values['minimum_goal'] = $options[$optionValues[1]];
			$values['timeframe'] = $options[$optionValues[2]];
			$values['weight'] = $options[$optionValues[3]];

			if (isset($optionValues[4]))
			{
				$values['options']['excluded_nodes'] = $options[$optionValues[4]];
			}

			if (isset($optionValues[5]))
			{
				$values['options']['include_comments'] = $options[$optionValues[5]];
			}
		}

		return $values;
	}

	/**
	 *
	 */
	protected function getHandlers()
	{
		$app = $this->app();

		$classes = [
			'apathy\DailyGoal\Goal\Post',
			'apathy\DailyGoal\Goal\Thread',
			'apathy\DailyGoal\Goal\User',
			'apathy\DailyGoal\Goal\DBTech\Purchase',
			'apathy\DailyGoal\Goal\Siropu\Message',
			'apathy\DailyGoal\Goal\Siropu\Shout',
			'apathy\DailyGoal\Goal\XenAddons\Ams',
			'apathy\DailyGoal\Goal\XenAddons\Classifieds',
			'apathy\DailyGoal\Goal\XenAddons\Ims',
			'apathy\DailyGoal\Goal\XenAddons\Link',
			'apathy\DailyGoal\Goal\XenAddons\Rms',
			'apathy\DailyGoal\Goal\XenAddons\Showcase',
			'apathy\DailyGoal\Goal\XenAddons\Ubs',
			'apathy\DailyGoal\Goal\XFRM\Resource',
		];

		$output = [];

		foreach ($classes AS $class)
		{
			if (class_exists($class))
			{
				$class = $app->extendClass($class);
				$output[] = new $class($app);
			}
		}

		return $output;
	}
}
