<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Install;

use XF\AddOn\AddOn;
use XF\App;
use XF\Db\Schema\Alter;
use XF\Db\Schema\Create;

/**
 * @property AddOn addOn
 * @property App app
 *
 * @method \XF\Db\AbstractAdapter db()
 * @method \XF\Db\SchemaManager schemaManager()
 * @method \XF\Db\Schema\Column addOrChangeColumn($table, $name, $type = null, $length = null)
 */

trait Table
{
	/**
	 * @return \Closure[]
	 */
	protected function getTables(): array
	{
		return ['xf_ap_daily_goal_goal' => function ($table)
		{
			/** @var Alter|Create $table */
			$table->addColumn('goal_id', 'int')->autoIncrement();
			$table->addPrimaryKey('goal_id');

			$table->addColumn('title', 'varchar', 30);
			$table->addColumn('fa_icon', 'varchar', 30);
			$table->addColumn('content_type', 'varchar', 30)->setDefault('');
			$table->addColumn('goal', 'int')->nullable()->setDefault(5);
			$table->addColumn('minimum_goal', 'int')->nullable()->setDefault(3);
			$table->addColumn('timeframe', 'int')->nullable()->setDefault(3);
			$table->addColumn('weight', 'int')->nullable()->setDefault(5);
			$table->addColumn('options', 'blob')->nullable()->setDefault(null);
			$table->addColumn('notification_posted', 'bool')->nullable()->setDefault(0);
			$table->addColumn('active', 'bool')->setDefault(1);
		}, 'xf_ap_daily_goal_history' => function ($table)
		{
			/** @var Alter|Create $table */
			$table->addColumn('goal_id', 'int')->autoIncrement();
			$table->addPrimaryKey('goal_id');

			$table->addColumn('date', 'int');
			$table->addColumn('content_type', 'varchar', 30);
			$table->addColumn('periodicity', 'varchar', 10)->setDefault('daily');
			$table->addColumn('counter', 'int')->nullable(true)->setDefault(null);
			$table->addColumn('goal', 'int')->nullable(true)->setDefault(null);
			$table->addColumn('fulfilled', 'bool')->setDefault(0);
		}];
	}
}
