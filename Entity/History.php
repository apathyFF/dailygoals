<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Entity;

use apathy\DailyGoal\Repository\Goal as GoalRepository;
use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;

/**
 * COLUMNS
 * @property int|null $goal_id
 * @property int      $date
 * @property string   $content_type
 * @property string   $periodicity
 * @property int      $counter
 * @property int      $goal
 * @property bool     $fulfilled
 *
 * RELATIONS
 * @property Goal $Goal
 */
class History extends Entity
{
	/**
	 * @return integer
	 */
	public function getStartDate(): int
	{
		return $this->getGoalRepo()->getTimeStartByPeriodicity($this->periodicity, $this->date);
	}

	/**
	 *
	 */
	protected function _postSave()
	{
		$this->getGoalRepo()->prepareSiropuNotification($this->Goal, $this->counter, 'reset');

		return true;
	}

	/**
	 * @param Structure $structure
	 *
	 * @return Structure
	 */
	public static function getStructure(Structure $structure): Structure
	{
		$structure->table = 'xf_ap_daily_goal_history';
		$structure->shortName = 'apathy\DailyGoal:History';
		$structure->primaryKey = 'goal_id';
		$structure->columns = [
			'goal_id' => [
				'type'          => self::UINT,
				'autoIncrement' => true,
				'nullable'      => true,
			],
			'date' => [
				'type'          => self::UINT,
				'default'       => \XF::$time,
			],
			'content_type' => [
				'type'          => self::STR,
				'maxLength'     => 30,
				'default'       => false,
			],
			'periodicity' => [
				'type'          => self::STR,
				'maxLength'     => 10,
				'default'       => 'daily',
			],
			'counter' => [
				'type'          => self::UINT,
				'default'       => 0,
			],
			'goal' => [
				'type'          => self::UINT,
				'default'       => 0,
			],
			'fulfilled' => [
				'type'          => self::BOOL,
				'default'       => 0,
			],
		];
		$structure->getters   = [
			'start_date' => true,
		];
		$structure->relations = [
			'Goal' => [
				'entity'     => 'apathy\DailyGoal:Goal',
				'type'       => self::TO_ONE,
				'conditions' => 'content_type',
			],
		];

		return $structure;
	}

	/**
	 * @return GoalRepository
	 */
	protected function getGoalRepo(): GoalRepository
	{
		return $this->repository('apathy\DailyGoal:Goal');
	}
}
