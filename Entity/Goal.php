<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Entity;

use apathy\DailyGoal\Goal\AbstractGoal;
use apathy\DailyGoal\Repository\Goal as RepositoryGoal;
use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;
use XF\Phrase;

/**
 * COLUMNS
 * @property int|null $goal_id
 * @property string   $title
 * @property string   $fa_icon
 * @property string   $content_type
 * @property int      $goal
 * @property int      $minimum_goal
 * @property int      $timeframe
 * @property int      $weight
 * @property array    $options
 * @property bool     $notification_posted
 * @property bool     $active
 *
 * GETTERS
 * @property string   $goal_phrase
 * @property string   $streak_phrase
 * @property string   $longest_streak_phrase
 * @property string   $simple_cache_id
 *
 * RELATIONS
 * @property History $History
 */

class Goal extends Entity
{
	/**
	 * @param array $time
	 *
	 * @return integer
	 */
	public function calculateGoalProgress(array $time): int
	{
		$handler = $this->getHandler();
		$progress = $handler->calculateProgressTowardsGoal($time['start'], $time['end']);

		$this->getGoalRepo()->prepareSiropuNotification($this, $progress);
		$this->setSimpleCacheValue($progress);

		return $progress;
	}

	/**
	 * @return boolean
	 */
	public function isActive(): bool
	{
		return ($this->getHandler() && $this->active && $this->getHandler()->isEnabled());
	}

	/**
	 * @return boolean
	 */
	public function isVisible(): bool
	{
		if (!$this->getHandler() || !$this->isActive())
		{
			return false;
		}

		return $this->getHandler()->getPermission();
	}

	/**
	 * @return AbstractGoal|null
	 */
	public function getHandler(): ?AbstractGoal
	{
		return $this->getGoalRepo()->getHandler($this->content_type, $this);
	}

	/**
	 * @return Phrase
	 */
	public function getGoalPhrase(): Phrase
	{
		return $this->getHandler()->getGoalPhrase();
	}

	/**
	 * @return Phrase
	 */
	public function getStreakPhrase(): Phrase
	{
		return $this->getHandler()->getStreakPhrase();
	}

	/**
	 * @return Phrase
	 */
	public function getLongestStreakPhrase(): Phrase
	{
		return $this->getHandler()->getLongestStreakPhrase();
	}

	/**
	 * @return string
	 */
	public function getSimpleCacheId(): string
	{
		return \XF::escapeString($this->getHandler()->getSimpleCacheId());
	}

	/**
	 * @return integer|null
	 */
	public function getSimpleCacheValue(): ?int
	{
		return $this->getHandler()->getProgressValueForWidget();
	}

	/**
	 * @param integer $progress
	 */
	public function setSimpleCacheValue(int $progress)
	{
		$this->app()->simpleCache['apathy/DailyGoal'][$this->simple_cache_id] = $progress;
	}

	/**
	 * @return array
	 */
	public function getStyleProperties(): array
	{
		return $this->getHandler()->getStyleProperties();
	}

	/**
	 * @return boolean
	 */
	public function hasTemplate(): bool
	{
		return $this->getHandler()->getTemplateParams() !== [];
	}

	/**
	 * @return string
	 */
	public function renderTemplate(): string
	{
		if (!$this->hasTemplate())
		{
			return '';
		}

		$handler = $this->getHandler();

		return $handler->renderTemplate($handler->getTemplate());
	}

	/**
	 *
	 */
	protected function _postDelete()
	{
		$this->getGoalRepo()->rebuildGoalStyleCache();
	}

	/**
	 *
	 */
	protected function _postSave()
	{
		if (!$this->getHandler() instanceof AbstractGoal)
		{
			return null;
		}

		$repo = $this->getGoalRepo();
		$time = $repo->getTime();

		$this->calculateGoalProgress($time);

		$repo->rebuildGoalStyleCache();

		return true;
	}

	/**
	 * @param Structure $structure
	 *
	 * @return Structure
	 */
	public static function getStructure(Structure $structure): Structure
	{
		$structure->table = 'xf_ap_daily_goal_goal';
		$structure->shortName = 'apathy\DailyGoal:Goal';
		$structure->primaryKey = 'goal_id';
		$structure->columns = [
			'goal_id' => [
				'type'          => self::UINT,
				'autoIncrement' => true,
				'nullable'      => true,
			],
			'title' => [
				'type'          => self::STR,
				'maxLength'     => 30,
				'required'      => 'please_enter_valid_title',
			],
			'fa_icon' => [
				'type'          => self::STR,
				'maxLength'     => 30,
			],
			'content_type' => [
				'type'          => self::STR,
				'maxLength'     => 30,
				'unique'        => 'ap_dg_this_goal_already_exists',
			],
			'goal' => [
				'type'          => self::UINT,
				'nullable'      => true,
				'default'       => 5,
			],
			'minimum_goal' => [
				'type'          => self::UINT,
				'nullable'      => true,
				'default'       => 3,
			],
			'timeframe' => [
				'type'          => self::UINT,
				'nullable'      => true,
				'default'       => 3,
			],
			'weight'  => [
				'type'          => self::UINT,
				'nullable'      => true,
				'default'       => 5,
			],
			'options' => [
				'type'          => self::JSON_ARRAY,
				'nullable'      => true,
				'default'       => null,
			],
			'notification_posted' => [
				'type'          => self::BOOL,
				'default'       => 0,
			],
			'active' => [
				'type'          => self::BOOL,
				'default'       => 1,
			],
		];
		$structure->getters = [
			'goal_phrase'           => true,
			'streak_phrase'         => true,
			'longest_streak_phrase' => true,
			'simple_cache_id'       => true,
		];
		$structure->relations = [
			'History' => [
				'entity'     => 'apathy\DailyGoal:History',
				'type'       => self::TO_MANY,
				'conditions' => 'content_type',
			],
		];

		return $structure;
	}

	/**
	 * @return RepositoryGoal
	 */
	protected function getGoalRepo(): RepositoryGoal
	{
		return $this->repository('apathy\DailyGoal:Goal');
	}
}
