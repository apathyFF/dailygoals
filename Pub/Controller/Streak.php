<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\Pub\Controller;

use apathy\DailyGoal\Repository\Goal;
use apathy\DailyGoal\XF\Entity\User;
use XF\Mvc\Entity\ArrayCollection;
use XF\Mvc\Reply\AbstractReply;
use XF\Phrase;
use XF\Pub\Controller\AbstractController;

/**
 * Class Streak
 *
 * @author apathy <https://fortreeforums.xyz>
 *
 * @package \apathy\DailyGoal\Pub\Controller
 */

class Streak extends AbstractController
{
	/**
	 * @param array $activities
	 *
	 * @return Phrase
	 */
	public static function getActivityDetails(array $activities): Phrase
	{
		return \XF::phrase('ap_dg_viewing_goal_streaks');
	}

	/**
	 * @return AbstractReply
	 */
	public function actionIndex()
	{
		/** @var User $visitor */
		$visitor = \XF::visitor();

		if (!$visitor->canViewStreaksPage())
		{
			return $this->noPermission();
		}

		$options = $this->app()->options();
		$page = $this->filterPage();
		$perPage = $options->apDgStreakPageLimit;

		/** @var Goal $goalRepo */
		$goalRepo = $this->repository('apathy\DailyGoal:Goal');
		$goals = $goalRepo->findActiveGoalsForList();

		$filters = $this->filterTypes($goals);

		$graphRange = (($options->apDgGraphRange * 86400) * 7);

		// Draw the graph
		$streakTypes = $this->findStreakTypes($goals);

		/** @var \apathy\DailyGoal\Repository\Streak $streakRepo */
		$streakRepo = $this->repository('apathy\DailyGoal:Streak');

		$history = $streakRepo->findGoalHistoryForGraph(
			$goalRepo->getTime()['start'],
			$graphRange,
			$filters
		);

		$streaks = $this->drawStreakGraph($goals, $history->fetch($graphRange));

		$history = $streakRepo->findGoalHistory('', true, true, $page);
		$history = $history->order('date', 'desc');

		$viewParams = [
			'pageTitle'     => $this->getTitle(),
			'goals'         => $goals,
			'history'       => $history->fetch(),
			'streaks'       => $streaks,
			'streakTypes'   => $streakTypes,
			'page'          => $page,
			'perPage'       => $perPage,
			'total'         => $history->total(),
		];

		return $this->view('apathy\DailyGoal:Streak', 'ap_dg_streaks', $viewParams);
	}

	/**
	 * @return Phrase
	 */
	protected function getTitle(): Phrase
	{
		switch ($this->options()->apDgPeriodicity)
		{
			default:
			case 'daily':   $periodicity = \XF::phrase('daily')->render();
				break;
			case 'weekly':  $periodicity = \XF::phrase('weekly')->render();
				break;
			case 'monthly': $periodicity = \XF::phrase('monthly')->render();
				break;
		}

		return \XF::phrase('ap_dg_periodicity_x_streaks', ['periodicity' => $periodicity]);
	}

	/**
	 * @param ArrayCollection $goals
	 * @param ArrayCollection $history
	 *
	 * @return array
	 */
	protected function drawStreakGraph(ArrayCollection $goals, ArrayCollection $history): array
	{
		$streaks = [];

		$values = $this->initializeValuesAndAverages($goals);

		foreach ($history AS $item)
		{
			$date = date('Y-m-d', $item['date']);
			$goal = $item->Goal;

			$type = $goal['content_type'];

			if ($item['content_type'] === $type && $goal->isActive() && $goal->isVisible())
			{
				$values[$type] = ($item['fulfilled']) ? ++$values[$type] : 0;

				$streaks[$date] = [
					'ts'       => $item['date'],
					'label'    => $date,
					'days'     => 1,
					'count'    => 1,
					'values'   => $values,
					'averages' => $values,
				];
			}
		}

		return $streaks;
	}

	/**
	 * @param ArrayCollection $goals
	 *
	 * @return array
	 */
	protected function filterTypes(ArrayCollection $goals): array
	{
		$filters = [];

		/** @var \apathy\DailyGoal\Entity\Goal $goal */
		foreach ($goals AS $goal)
		{
			if ($goal->isActive() && $goal->isVisible())
			{
				$filters[] = ['content_type', $goal['content_type']];
			}
		}

		return $filters;
	}

	/**
	 * @param ArrayCollection $goals
	 *
	 * @return array
	 */
	protected function findStreakTypes(ArrayCollection $goals): array
	{
		$streakTypes = [];

		/** @var \apathy\DailyGoal\Entity\Goal $goal */
		foreach ($goals AS $goal)
		{
			if ($goal->isActive() && $goal->isVisible())
			{
				$type = $goal['content_type'];
				$streakTypes[$type] = $goal->streak_phrase;
			}
		}

		return $streakTypes;
	}

	/**
	 * @param ArrayCollection $goals
	 *
	 * @return array
	 */
	protected function initializeValuesAndAverages(ArrayCollection $goals): array
	{
		$values   = [];

		/** @var \apathy\DailyGoal\Entity\Goal $goal */
		foreach ($goals AS $goal)
		{
			if ($goal->isActive() && $goal->isVisible())
			{
				$type = $goal['content_type'];

				$values[$type] = 0;
			}
		}

		return $values;
	}
}
