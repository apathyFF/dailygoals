<?php


/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\XF\Entity;

class User extends XFCP_User
{
	/**
	 * @return boolean
	 */
	public function canViewGoalWidget(): bool
	{
		return $this->hasPermission('apDailyGoals', 'viewWidget');
	}

	/**
	 * @param string $permissionId
	 *
	 * @return boolean
	 */
	public function canViewGoal(string $permissionId): bool
	{
		return $this->hasPermission('apDailyGoals', $permissionId);
	}

	/**
	 * @return boolean
	 */
	public function canViewStreaksPage(): bool
	{
		return $this->hasPermission('apDailyGoals', 'viewStreaks');
	}

}
