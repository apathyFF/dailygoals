<?php

/**
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Daily Goals ("Daily Goals").
 *
 *  Daily Goals is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Daily Goals is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Daily Goals.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\DailyGoal\XF;

class CssRenderer extends XFCP_CssRenderer
{
	/**
	 * @return array
	 */
	protected function getRenderParams()
	{
		$params = parent::getRenderParams();

		if ($this->includeExtraParams)
		{
			try
			{
				$params['apDgGoalStyles'] = $this->app->container('ap.dgGoalStyles');
			}
			catch (\Exception $e)
			{
				$params['apDgGoalStyles'] = [];
			}
		}

		return $params;
	}
}
